from numbers import  Number


def square(num: Number) -> Number:
    return num ** 2


def say_hi(txt: str) -> str:
    print("Hi, ", txt)


def cube(num: Number) -> Number:
    if not isinstance(num, Number):
        raise TypeError(f"expected type Number or got {type(num)}")
    return pow(num, 3)

